#!/usr/bin/env python

"""

Copyright (C) 2008 by Florian Hasheider
florian.hasheider@googlemail.com

Copyright (C) 2008 by Benjamin Kircher
benjamin.kircher@gmail.com

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

"""

from map import *


__doc__ = """

TODO: add description
"""


class Unit:
    """Abstract base class for all units.
    """

    def __init__(self, pos):
        if self.__class__ is Unit:
            raise NotImplementedError
        self.pos = pos or (0, 0)
        self.allowed_tiles = (Plain, Mountain, Forest, River)
        self.strength = ()


class MountedUnit(Unit):
    """Abstract base class for all mounted units.
    """

    def __init__(self, pos):
        if self.__class__ is MountedUnit:
            raise NotImplementedError
        Unit.__init__(self, pos)
        self.allowed_tiles = (Plain, Forest, River)


class SeaUnit(Unit):
    """Abstract base class for all naval units.
    """

    def __init__(self, pos):
        if self.__class__ is SeaUnit:
            raise NotImplementedError
        Unit.__init__(self, pos)
        self.allowed_tiles = (Sea)


class Archer(Unit):
    def __init__(self, pos):
        Unit.__init__(self, pos)
        self.strength = (50, 0, 100)


class Swordsman(Unit):
    def __init__(self, pos):
        Unit.__init__(self, pos)
        self.strength = (100, 50, 0)


class Explorer(Unit):
    def __init__(self, pos):
        Unit.__init__(self, pos)
        self.strength = (0, 50, 0)


class Superman(Unit):
    def __init__(self, pos):
        Unit.__init__(self, pos)
        self.strength = (500, 500, 500)
        self.allowed_tiles = (Plain, Mountain, Forest, River, Sea)


class Knight(MountedUnit):
    def __init__(self, pos):
        MountedUnit.__init__(self, pos)
        self.strength = (150, 50, 0)


class Sailboat(SeaUnit):
    def __init__(self, pos):
        SeaUnit.__init__(self, pos)
        self.strength = (50, 50, 50)


# ----------------------------------- TESTS ---------------------------------- #

import unittest

class ModuleTest(unittest.TestCase):
    """Test case for military units.
    """

    def setUp(self):
        """Test correct instantiation of units.

        TODO: add more units
        """
        # foot soldiers
        self.archer = Archer((0, 0))
        self.swordsman = Swordsman((0, 0))

        # special units
        self.explorer = Explorer((0, 0))
        self.superman = Superman((0, 0))

        # mounted units
        self.knight = Knight((0, 0))

        # naval forces
        self.sailboat = Sailboat((0, 0))


    def test_abstract(self):
        """Ensure that abstract units cannot be instantiated.

        Abstract units are generic units, mounted units and sea units.
        """
        self.failUnlessRaises(NotImplementedError, Unit, (0, 0))
        self.failUnlessRaises(NotImplementedError, MountedUnit, (0, 0))
        self.failUnlessRaises(NotImplementedError, SeaUnit, (0, 0))

    def test_position(self):
        """Test position attribute of units."""
        pass

    def test_strength(self):
        """Ensure that each unit has appropriate strength tuple.
        
        The strength of a unit consists usually of three values:
            (attack, defense, bombing)
        """
        self.failUnlessEqual((50, 0, 100), self.archer.strength)
        self.failUnlessEqual((100, 50, 0), self.swordsman.strength)
        self.failUnlessEqual((0, 50, 0), self.explorer.strength)
        self.failUnlessEqual((500, 500, 500), self.superman.strength)
        self.failUnlessEqual((150, 50, 0), self.knight.strength)
        self.failUnlessEqual((50, 50, 50), self.sailboat.strength)

    def test_allowed_tiles(self):
        """Ensure that each unit has appropriate allowed_tiles tuple."""
        self.failUnlessEqual((Plain, Mountain, Forest, River),
            self.archer.allowed_tiles)
        self.failUnlessEqual((Plain, Forest, River), self.knight.allowed_tiles)
        self.failUnlessEqual((Sea), self.sailboat.allowed_tiles)
        self.failUnlessEqual((Plain, Mountain, Forest, River, Sea),
            self.superman.allowed_tiles)


def main():
    unittest.main()

if __name__ == '__main__':
    main()

