#!/usr/bin/env python

"""

Copyright (C) 2008 by Florian Hasheider
florian.hasheider@googlemail.com

Copyright (C) 2008 by Benjamin Kircher
benjamin.kircher@gmail.com

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

"""

import sys, pygame


__doc__ = """

TODO: add description
"""

pygame.init()

size = width, height = 320, 240
black = 0, 0, 0
white = 255, 255, 255

screen = pygame.display.set_mode(size)

"""
def draw_grid(screen):
    # create background
    background = pygame.Surface(screen.get_size())
    background = background.convert()
    background.fill(black)

    # draw lines
    d = pygame.draw
    for y in range(9):
        d.line(background, white, (0, y*10), (399, y*10), 1)
    for x in range(9):
        d.line(background, white, (x*10, 0), (x*10, 399), 1)

    screen.blit(background, (0, 0))


while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()

    draw_grid(screen)
    pygame.display.flip()
"""


# ----------------------------------- TESTS ---------------------------------- #

import unittest

class ModuleTest(unittest.TestCase):
    """Main test case for this module.
    """
    pass


def main():
    unittest.main()

if __name__ == '__main__':
    main()

