#!/usr/bin/env python

"""

Copyright (C) 2008 by Florian Hasheider
florian.hasheider@googlemail.com

Copyright (C) 2008 by Benjamin Kircher
benjamin.kircher@gmail.com

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

"""

import types
import csv


__doc__ = """

TODO: add description
"""

class Map:
    """
    """

    def __init__(self, width, height):
        self.width = width
        self.height = height


class Tile:
    """Abstract base class for various tiles.
    """

    def __init__(self):
        if self.__class__ is Tile:
            raise NotImplementedError
        self.fogofwar = True
        self.units = []

        # path to BMP file
        name = str(self.__class__).split('.')[-1].lower()
        self.image = "data/images/" + name + ".bmp"


class Sea(Tile):
    def __init__(self):
        Tile.__init__(self)


class Plain(Tile):
    def __init__(self):
        Tile.__init__(self)
        self.city = None


class Mountain(Tile):
    def __init__(self):
        Tile.__init__(self)
        self.mine = None


class Forest(Tile):
    def __init__(self):
        Tile.__init__(self)
        self.watchtower = None


class River(Tile):
    def __init__(self):
        Tile.__init__(self)
        self.bridge = None


MAPPING = {'.': Sea, '_': Plain, 'M': Mountain, '^': Forest, '~': River}

def load_map(filename):
    """Load map from given filename. Returns a map object.
    """
    if type(filename) == types.ListType:
        f = [line.strip().split(',') for line in filename]
    else:
        f = list(csv.reader(open(filename, 'r'), delimiter=','))
    return Map(width=len(f), height=len(f[0]))


# ----------------------------------- TESTS ---------------------------------- #

import unittest

class ModuleTest(unittest.TestCase):
    """Main test case for this module.
    """

    def test_mapping(self):
        """Ensure correct mapping of tile symbols."""
        expected = {'.': Sea, '_': Plain, 'M': Mountain, '^': Forest,
            '~': River}
        self.failUnlessEqual(expected, MAPPING)

    def test_load_map_from_file(self):
        """Test correct loading of maps from CSV files."""
        map = load_map('data/maps/empty.map.csv')
        self.failUnlessEqual((16, 16), (map.width, map.height))

    def test_load_map_from_list(self):
        """Test correct loading of maps from lists."""
        # empty 16x16 map, all tiles are sea
        empty_map = [
            '.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.',
            '.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.',
            '.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.',
            '.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.',
            '.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.',
            '.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.',
            '.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.',
            '.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.',
            '.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.',
            '.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.',
            '.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.',
            '.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.',
            '.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.',
            '.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.',
            '.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.',
            '.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.',
        ]
        map = load_map(empty_map)
        self.failUnlessEqual((16, 16), (map.width, map.height))

class MapTest(unittest.TestCase):
    def test_init(self):
        """Test correct instantiation of maps."""
        map = Map(8, 8)
        self.failUnlessEqual((8, 8), (map.width, map.height))


class TileTest(unittest.TestCase):
    def test_abstract(self):
        """Ensure that tile class is abstract."""
        self.failUnlessRaises(NotImplementedError, Tile)


class RiverTest(unittest.TestCase):
    def setUp(self):
        self.river = River()

    def test_init(self):
        """Test correct instantiation of rivers."""
        self.failUnlessEqual(self.river.fogofwar, True)
        self.failUnlessEqual(self.river.bridge, None)
        self.failUnlessEqual(self.river.units, [])

    def test_image(self):
        """Test if image string is correct."""
        self.failUnlessEqual("data/images/river.bmp", self.river.image)


class SeaTest(unittest.TestCase):
    def setUp(self):
        self.sea = Sea()

    def test_init(self):
        """Test correct instantiation of sea tiles."""
        self.failUnlessEqual(self.sea.fogofwar, True)
        self.failUnlessEqual(self.sea.units, [])

    def test_image(self):
        """Test if image string is correct."""
        self.failUnlessEqual("data/images/sea.bmp", self.sea.image)


class PlainTest(unittest.TestCase):
    def setUp(self):
        self.plain = Plain()

    def test_init(self):
        """Test correct instantiation of plains."""
        self.failUnlessEqual(self.plain.fogofwar, True)
        self.failUnlessEqual(self.plain.units, [])
        self.failUnlessEqual(self.plain.city, None)

    def test_image(self):
        """Test if image string is correct."""
        self.failUnlessEqual("data/images/plain.bmp", self.plain.image)

class MountainTest(unittest.TestCase):
    def setUp(self):
        self.mountain = Mountain()

    def test_init(self):
        """Test correct instantiation of mountains."""
        self.failUnlessEqual(self.mountain.fogofwar, True)
        self.failUnlessEqual(self.mountain.mine, None)
        self.failUnlessEqual(self.mountain.units, [])

    def test_image(self):
        """Test if image string is correct."""
        self.failUnlessEqual("data/images/mountain.bmp", self.mountain.image)


class ForestTest(unittest.TestCase):
    def setUp(self):
        self.forest = Forest()

    def test_init(self):
        """Test correct instantiation of forest tiles."""
        self.failUnlessEqual(self.forest.fogofwar, True)
        self.failUnlessEqual(self.forest.watchtower, None)
        self.failUnlessEqual(self.forest.units, [])

    def test_image(self):
        """Test if image string is correct."""
        self.failUnlessEqual("data/images/forest.bmp", self.forest.image)


def main():
    unittest.main()

if __name__ == '__main__':
    main()

